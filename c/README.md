# Words with my Terminal

This version of the problem introduces a few new features to your scrabble
program. Now the program checks if the word is valid according to the
(non-exhaustive) `word-lists.txt`.

If you take a look at the [word.c](src/word.c) file, you'll see that some of
the functions are missing an implementation. The first task is to fill them
out.

# Directories

1. src/
   - Contains all of the implementation files for the program. These source
     files typically `#include` a set of header files (ending in `.h`):
2. include/
   - Contains the public interfaces to the objects (structs) in the program.
2. tests/
   - A directory containing (currently) one test file that tests some of the
     word.h functions.

# A Note on the Implementation

The UI is implemented using a library called [ncurses](https://linux.die.net/man/3/ncurses).
The library is designed for "drawing" to terminals, and is used by lots of
terminal programs (`irssi`, `htop`, `nano`, etc).

The program uses a decoupled architecture, where the main loop
polls the ui for user input, updates the internal state (captured largely
in the `board.h/c` files) and refreshes the ui display to show the updated
state. This structure and encapsulation of concepts and function is typical
for a user driven program and facilitates extension and maintenance.

# Building the Source

There are a few targets in the [Makefile](Makefile), namely:

- `all / release` - these are equivalent and build the "release" binary
- `test`          - this target builds the tests, and runs them for convienience
                    note that the tests are built with debug symbols, so you
                    should be able to step through them with `gdb` (there are
                    config files included for vscode to make debugging easier
                    to set up)
- `clean`         - this target deletes all of the build data
- `debug`         - this builds the release binary with debug symbols

To run any of these targets, just do:
```
  make <target>
```

# Running the program

Once you've built the program, and implemented the functions in [word.c](src/word.c),
you should be able to run the release binary and do some testing:
```
./words-with-my-terminal
```
You should see a scrabble board, with a rack of tiles. To move the cursor, use the
arrow keys.

You can try to place tiles by typing the letter you want. If it's in
your rack (or you have a wildcard `_` tile), it will be added to the board. Once
you've completed your turn, press enter - you will then be scored or told that the
turn was invalid.

If you want to undo the current turn, press `ESC`, or you can press `DEL` to undo
individual tile placements.

You can enter command mode (stolen from `vim`/`less`/etc) by typing the `:`
character. Once in command mode, you currently have 3 options:
- `:q`  - quits the game
- `:s`  - shuffles your current tile rack, picking fresh tiles from the "bag"
- `ESC` - exits command mode and returns you to the board

This hasn't been extensively tested, so if you find a bug in the program as
implemented in the [ct-starter-guide](https://gitlab.com/ct-starter-guide/project2),
please file an issue there, and ideally find and fix the problem.

# Things to add

This implementation is bare bones. There is lots of scope for improvement.
Here's a list of things that you could consider:

## Big Stuff
- __Multiplayer__ - you could add support for multiple players
- __Networking__ - you could connect to another instance of this program
  peer-to-peer and play over a network with another player. You would have
  to design a simple protocol to express the turns, and have some way of sharing
  the board and tile states.
- __Scrolling__ - in undersized terminals it would be nice if you could scroll the
  display.
- __Proper GUI__ - the current implementation uses ncurses, you could swap out the
  UI for something truly graphical (look at SDL2, qt or gtk for possible libraries).
- __Improve command mode__ - currently the command mode just takes one letter (it
  allows multiple letters, but doesn't parse them) - this could be easily extended
  to take strings of commands (like vim), or at least to handle the user input more
  gracefully.
- __End Game__ - there is no logic here for the endgame (when you run out of tiles
  and have to tot up your score, minus the value of the unused tiles). There is a
  `tile_bag_empty` function already implemented that should be useful.
- __Help Function__ - the `:h` command could be added, showing the user interface
  and explaining the rules of the game.
- __More Tests__ - there aren't enough unit tests.

## Niggles
- Allow for reordering of the tiles on the rack
- Would be nice if the scoring was shown (maybe on the right hand side of the board)
  for each successful turn, with each word shown (with their constituent tile values).
- The program could take command-line arguments that start it in a different
  configuration - perhaps you could make the word_list file configurable, or support
  different width boards.

Note that there are some `TODO`s in the code itself (`git grep TODO` to
find them).
