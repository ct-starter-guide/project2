#include <stdio.h>

#include "board.h"

#define BOARD_MID (BOARD_MAX_WIDTH / 2) + 1

static board_square_t board_square[BOARD_MAX_WIDTH * BOARD_MAX_WIDTH] = {0};
static const board_square_type_e board_square_type[BOARD_MID][BOARD_MID] =
{
	{'t', '-', '-', '2', '-', '-', '-', 't'},
	{'-', 'd', '-', '-', '-', '3', '-', '-'},
	{'-', '-', 'd', '-', '-', '-', '2', '-'},
	{'2', '-', '-', 'd', '-', '-', '-', '2'},
	{'-', '-', '-', '-', 'd', '-', '-', '-'},
	{'-', '3', '-', '-', '-', '3', '-', '-'},
	{'-', '-', '2', '-', '-', '-', '2', '-'},
	{'t', '-', '-', '2', '-', '-', '-', '*'}
};

static board_t board =
{
	.square = board_square,
	.width  = BOARD_MAX_WIDTH
};

#define BOARD_SQUARE_FROM_POS(pos) board.square[(pos.y * board.width) + pos.x]

/* Return tile at pos */
static inline tile_t* board_get_tile(position_t *pos)
{
	return (pos && board_valid_position(*pos)) ?
		&(BOARD_SQUARE_FROM_POS((*pos)).tile)
		: NULL;
}

typedef enum
{
	BOARD_STATE_FIRST_TURN,
	BOARD_STATE_PLAYING,
	BOARD_STATE_END,
} board_state_e;

static board_state_e board_state = BOARD_STATE_FIRST_TURN;


void board_init(unsigned width)
{
	// TODO: support variable width boards
	if (width != board.width) return;
	unsigned board_mid = (board.width / 2) + 1;
	unsigned type_row, type_col;

	for (unsigned row = 0; row < board.width; row++)
	{
		for (unsigned col = 0; col < board.width; col++)
		{
			type_row = (row >= board_mid) ? (2 * (board_mid - 1)) - row : row;
			type_col = (col >= board_mid) ? (2 * (board_mid - 1)) - col : col;

			board.square[(row * board.width) + col].type =
				board_square_type[type_row][type_col];
		}
	}
}


unsigned board_get_width(void)
{
	return board.width;
}


static int sign(int num)
{
	if (num < 0) return -1;
	if (num > 0) return 1;
	return 0;
}


bool board_valid_position(position_t position)
{
	return (position.x < board.width) && (position.y < board.width);
}


/* Turn placement and deleteion stacks */
#define BOARD_MAX_DIFF 16
static position_t board_diff[BOARD_MAX_DIFF] = {0};
static int board_diff_index = -1;
static position_t board_deletes[BOARD_MAX_DIFF] = {0};
static int board_deletes_index = -1;


bool board_add_tile(tile_t     tile,
                    position_t position)
{
	tile_t* board_tile = board_get_tile(&position);

	if (!board_tile || tile_occupied(board_tile))
		return false;

	if ((board_diff_index + 1 < BOARD_MAX_DIFF) && (board_diff_index >= -1))
		board_diff[++board_diff_index] = position;
	else
		return false;

	*board_tile = tile;
	return true;
}


board_square_t* board_get_square(position_t position)
{
	if (!board_valid_position(position))
		return NULL;
	return &(board.square[(position.y * board.width) + position.x]);
}


/* Given start and end positions, return a word (if there is one) */
board_word_t board_word_from_line(position_t start, position_t end)
{
	if (!board_valid_position(start) || !board_valid_position(end)
	    || position_lt(end, start))
		return word_empty;

	board_word_t word = {0};

	int dx = sign((int)end.x - (int)start.x);
	int dy = sign((int)end.y - (int)start.y);

	if (((dx != 0) && (dy != 0)) || (dx < 0) || (dy < 0))
		return word_empty;

	unsigned i;
	for (i = 0; (i < WORD_MAX_LENGTH) && (!position_lt(end, start)); i++)
	{
		word.square[i] = *board_get_square(start);
		start.x += dx;
		start.y += dy;
	}
	word.length = i;

	return word;
}


void board_turn_complete(void)
{
	if (board_diff_index >= 0)
	{
		for (int i = 0; i <= board_diff_index; i++)
		{
			position_t diff_pos = board_diff[i];
			if (!board_valid_position(diff_pos))
				continue;
			/* Remove multiplier for future words */
			BOARD_SQUARE_FROM_POS(diff_pos).type = '-';
		}
	}
	board_diff_index = -1;

	if (board_state == BOARD_STATE_FIRST_TURN)
		board_state = BOARD_STATE_PLAYING;
}


unsigned board_turn_length(void)
{
	return board_diff_index + 1;
}


bool board_turn_get_index(int index, board_square_t* square, position_t* position)
{
	if ((index > board_diff_index) || (index < 0)) return false;

	position_t diff_pos = board_diff[index];

	if (!board_valid_position(diff_pos)) return false;

	if (square)
		*square   = BOARD_SQUARE_FROM_POS(diff_pos);
	if (position)
		*position = diff_pos;

	return true;
}


bool board_turn_pop_position(position_t position, board_square_t* square)
{
	if (!square || !board_valid_position(position))
		return false;

	position_t diff_pos;
	bool in_turn = false;
	for (int i = 0; i <= board_diff_index; i++)
	{

		if (position_equal(board_diff[i], position))
		{
			in_turn  = true;
			diff_pos = board_diff[i];
		}

		if (in_turn && ((i + 1) < BOARD_MAX_DIFF))
			board_diff[i] = board_diff[i + 1];
	}

	if (in_turn)
	{
		board_diff_index--;
		*square = BOARD_SQUARE_FROM_POS(diff_pos);
		BOARD_SQUARE_FROM_POS(diff_pos).tile = as_tile(0);
		board_deletes[++board_deletes_index] = diff_pos;
	}

	return in_turn;
}


bool board_turn_pop(board_square_t* square, position_t* position)
{
	if (board_diff_index < 0) return false;

	position_t diff_pos = board_diff[board_diff_index--];

	if (!board_valid_position(diff_pos)) return false;

	if (square)
		*square   = BOARD_SQUARE_FROM_POS(diff_pos);
	if (position)
		*position = diff_pos;

	BOARD_SQUARE_FROM_POS(diff_pos).tile = as_tile(0);
	board_deletes[++board_deletes_index] = diff_pos;

	return true;
}


bool board_turn_deletes_pop(board_square_t* square, position_t* position)
{
	if (board_deletes_index < 0) return false;

	position_t del_pos = board_deletes[board_deletes_index--];
	if (square)
		*square   = BOARD_SQUARE_FROM_POS(del_pos);
	if (position)
		*position = del_pos;
	return true;
}


/* Stretches a line to include any adjacent tiles already on the board
 * in the direction of the word line */
static bool board_turn__stretch_line(
	position_t* start, position_t* end, int dx, int dy)
{
	if (!start || !end) return 0;

	unsigned increments = 0;

	position_t current = *start;
	while (board_valid_position(current)
	       && (BOARD_SQUARE_FROM_POS(current).tile.face != 0))
	{
		*start     = current;
		current.x -= dx;
		current.y -= dy;
		increments++;
	}

	current = (position_t) *end;
	while (board_valid_position(current)
	       && (BOARD_SQUARE_FROM_POS(current).tile.face != 0))
	{
		*end       = current;
		current.x += dx;
		current.y += dy;
		increments++;
	}

	return (increments > 2);
}


bool board_turn_get_words(board_word_t* words, size_t* words_len)
{
	if (!words || (*words_len == 0) || (board_diff_index < 0))
		return false;

	size_t words_index = 0;

	/* Find min and max tiles in principle line */
	position_t start = board_diff[0];
	position_t end   = board_diff[0];

	int dx = 0, dy = 0;

	for (int i = 1; i <= board_diff_index; i++)
	{
		position_t diff_pos = board_diff[i];
		if (!board_valid_position(diff_pos))
			return false;
		/* Ignore deletes / undos */
		if (BOARD_SQUARE_FROM_POS(diff_pos).tile.face == '\0')
			continue;

		if ((diff_pos.x < start.x) || (diff_pos.y < start.y))
			start = diff_pos;
		if ((diff_pos.x > end.x) || (diff_pos.y > end.y))
			end = diff_pos;
		dx = sign((int)end.x - (int)start.x);
		dy = sign((int)end.y - (int)start.y);
		/* Indicates invalid turn */
		if (((dy != 0) && (dx != 0)) || (dx < 0) || (dy < 0))
			return false;
	}

	/* Pick a principle direction for single tile placement
	 * if it's a 1 tile turn */
	if (board_diff_index == 0)
		dx = 1;
	else if ((dy == 0) && (dx == 0))
		return false;
	/* Search along principle line to extent */
	bool stretched = board_turn__stretch_line(&start, &end, dx, dy);

	words[words_index++] = board_word_from_line(start, end);

	/* Extend any strings perpendicular to placement */
	bool       center_square_placed = false;
	position_t center_pos           =
		(position_t) { .x = board.width / 2, .y = board.width / 2 };
	position_t perp_start = {0}, perp_end = {0};
	for (int i = 0; i <= board_diff_index; i++)
	{
		position_t diff_pos = board_diff[i];
		if (!board_valid_position(diff_pos))
			return false;

		if (position_equal(center_pos, diff_pos))
			center_square_placed = true;

		perp_start = diff_pos;
		perp_end   = diff_pos;

		if (BOARD_SQUARE_FROM_POS(diff_pos).tile.face == '\0')
			continue;
		stretched |= board_turn__stretch_line(&perp_start, &perp_end, dy, dx);

		if (position_lt(perp_start, perp_end))
		{
			if (words_index < *words_len)
				words[words_index++] = board_word_from_line(
					perp_start, perp_end);
			else
				return false;
		}
	}

	/* Validate placement of tiles */
	bool valid_placement = false;
	switch (board_state)
	{
		case BOARD_STATE_FIRST_TURN:
			valid_placement = center_square_placed;
			break;

		case BOARD_STATE_PLAYING:
			if (stretched)
				valid_placement = true;
			else
			{
				/* Check to see if placed tiles form contiguous line, with at
				 * least one tile being already on the board */
				for (position_t current = start;
				     !position_lt(end, current);
				     current.x += dx, current.y += dy)
				{
					if (!board_valid_position(current)
					    || (BOARD_SQUARE_FROM_POS(current).tile.face == '\0'))
					{
						valid_placement = false;
						break;
					}
					else
					{
						/* See if position is *not* in diff */
						bool in_diff = false;
						for (int i = 0; i <= board_diff_index; i++)
						{
							if (position_equal(board_diff[i], current))
								in_diff = true;
						}
						valid_placement |= !in_diff;
					}
				}
			}
			break;

		default:
			valid_placement = false;
			break;
	}

	if (!valid_placement)
		return false;

	*words_len = words_index;

	return true;
}
